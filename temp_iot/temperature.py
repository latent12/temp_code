from ...protocols import i2c_pb2
from ...common import krpc as implementations
from ...common import config
from ..polling_sensor import PollingSensor

_DEVICE_ADDR = 0x48
_SCALE_FACTOR = 0.0625

class TemperatureSensor(PollingSensor):
    value_type = float
    sensor_type = PollingSensor.SENSOR_TYPES.TEMPERATURE
    i2c = None

    def get_sensor_value(self):
        transaction = i2c_pb2.I2cTransaction()

        command = transaction.commands.add()
        command.action = command.READ_WORD_DATA
        command.addr = _DEVICE_ADDR
        command.cmd = 0x00

        controller = implementations.create_controller()
        results = self.i2c.Transaction(controller, transaction, None)

        if results != None and len(results.data_read) == len(transaction.commands):
            data = self._swap_word_bytes(results.data_read[0])
            return (data >> 4) * 0.0625
        else:
            print "Problem with response from i2c service: %s" % results
            return None

    def _swap_word_bytes(self, word):
        lsb = (word & 0xFF00) >> 8
        msb = word & 0xFF
        return (msb << 8) | lsb

    def additional_setup(self):
        i2c_host = config.get('TemperatureSensor', 'I2cHost')
        i2c_port = config.get('I2c', 'Port')

        channel = implementations.insecure_channel(i2c_host, i2c_port)
        self.i2c = i2c_pb2.I2cService_Stub(channel)

def main():
    config.init('sensor_temperature')

    sensor = TemperatureSensor()
    sensor.run()

if (__name__ == "__main__"):
    main()
