from ..protocols import heartbeat_pb2
from ..protocols import collector_pb2
from ..common import krpc as implementations
from ..common import sensor_types, config

from threading import Thread
from Queue import Queue

import pymongo
import time

"""The heartbeat server implements the server side of the heartbeat service."""

class HeartbeatService(heartbeat_pb2.HeartbeatService):
    received_cb = None

    def SetReceivedCallback(self, cb):
        """ Registers the callback for when a new heartbeat is received.
        Note that the callback will be called from a green thread
        inside the networking library."""
        self.received_cb = cb

    def Heartbeat(self, rpc_controller, request, context):
        """ Handle the Heartbeat method in the HeartbeatService interface,
        hand the request over to the callback waiting for it,
        and respond with a HeartbeatReply message. """

        print "Received a heartbeat from %s" % request.hostname
        if self.received_cb != None:
            self.received_cb(request)

        return heartbeat_pb2.HeartbeatReply(server_timestamp = int(time.time()))

class HeartbeatSender():
    """ A base class for different Heartbeat Sender implementations.

    It provides a thread and callback for receiving heartbeats, and calls
    the implementation's Send() method when required.

    Implementations must take care to implement Send() and to call the constructor if overriding.
    """
    q_received = None
    running = False
    send_thread = None

    def __init__(self):
        """ Initialize the received-queue. """
        self.q_received = Queue()

    def ReceivedCallback(self, heartbeat):
        """ Put the heartbeat into the received-queue """
        self.q_received.put(heartbeat)

    def RunSend(self):
        """ A thread for processing the received-queue. Calls the Send
        implementation for each item."""
        while self.running:
            heartbeat = self.q_received.get()
            try:
                self.Send(heartbeat)
            except Exception as e:
                print "Could not send heartbeat onwards. Discarding."
                print e.message

    def Start(self):
        """ Spawn the sender thread if not already running. """
        if not self.running:
            self.running = True
            self.send_thread = Thread(target=self.RunSend)
            self.send_thread.start()

    def Send(self, heartbeat):
        print "Implementations of HeartbeatSender must override Send()."
        raise NotImplementedError()


class MongoHeartbeatSender(HeartbeatSender):
    """ The MongoDB implementation of the HeartbeatSender inserts records directly into the
    NodeCollection and updates timestamps and sensor types in them. A historical record is
    not created."""
    db = None

    def __init__(self, mongo_host, mongo_port, mongo_database):
        """ Initialize a MongoClient instance with the given connection information. """

        # We must manually call the parent class constructor in python!
        HeartbeatSender.__init__(self)

        self.db = pymongo.MongoClient(mongo_host, mongo_port)[mongo_database]

    def Send(self, heartbeat):
        """ Updates the heartbeat record in MongoDB to reflect the new timestamp and possibly
        updated sensor type list.
        """
        sensors = []

        # To merge the set of sensors in the database with that received, we have to create
        # a list of the short sensor names as strings. The heartbeat message type stored
        # the sensor types as an enum.
        for sensor in heartbeat.sensors:
            sensors.append(sensor_types.sensor_type_name(sensor))

        # Update the heartbeat record found by the unique _id based on the hostname.
        # The timestamps are updated ($set). `upsert` is set to insert a new record
        # instead, if no previous record with the given _id exists.
        self.db.NodeCollection.update_one(
            {'_id' : heartbeat.hostname, 'name': heartbeat.hostname},
            {
                '$set': {
                    'heartbeat_timestamp_local' : heartbeat.timestamp,
                    'heartbeat_timestamp' : int(time.time()),
                    'meta' : {
                        'location' : heartbeat.meta.location
                    }
                },
                '$addToSet': {'sensor_types': { '$each': sensors}}
            },
            upsert=True)

def main():
    # Get configuration values
    config.init('heartbeat')
    port = (config.get('Heartbeat', 'Host'), config.getint('Heartbeat', 'Port'))
    mongo_host = config.get('Mongo', 'Host')
    mongo_port = config.getint('Mongo', 'Port')
    mongo_database = config.get('Mongo', 'Database')

    # Start the sender threads connecting to MongoDB
    sender = MongoHeartbeatSender(mongo_host, mongo_port, mongo_database)
    sender.Start()

    # Initialize the RPC servicer for the HeartbeatService
    servicer = HeartbeatService()
    servicer.SetReceivedCallback(sender.ReceivedCallback)

    # Start listening on the TCP port.
    print "Heartbeat service listening on %s:%d." % port
    server = implementations.create_server(servicer)
    server.add_insecure_port('%s:%d' % port)
    server.serve_forever()

if (__name__ == "__main__"):
    main()
