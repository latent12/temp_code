from ..protocols import collector_pb2
from ..common import sensor_types
from ..common import config
import redis
import requests
import time
import hmac
import hashlib

def main():
    config.init('upload')
    redis_host = config.get('Redis', 'Host')
    redis_port = config.getint('Redis', 'Port')
    upload_url = config.get('Upload', 'Url')
    secret_key = config.get('Upload', 'SecretKey')
    upload_interval = config.getint('Upload', '5')
    last_upload = 0;
    data = dict()

    r = redis.StrictRedis(host=redis_host, port=redis_port)
    p = r.pubsub()
    p.psubscribe("sensor_status.*")

    for message in p.listen():
        if message['type'] not in ['message', 'pmessage']:
            continue

        try:
            status = collector_pb2.SensorStatus.FromString(message['data'])
            hostname = status.hostname
            sensor_type = sensor_types.sensor_type_name(status.sensor_type)

            key = "%s/%s" % (hostname, sensor_type)

            sensor_value = str(round(status.float_value, 2) or status.int_value or status.bool_value)
            data[key] = "%s,%s" % (status.timestamp, sensor_value)
        except Exception as e:
            # TODO proper error handling, assume that anything going wrong here is due to wrong message types.
            print e
            continue

        if (time.time() - last_upload > upload_interval):

            signature = sign_data(data, secret_key)
            url = '%s/%s' % (upload_url, signature)

            req = requests.post(url, data=data)
            last_upload = time.time()

            if (req.status_code == 200):
                print "data uploaded"
            else:
                print "error uploading (%d)" % req.status_code
            print req.text

def sign_data(data, secret_key):
    keys = data.keys()
    keys.sort()

    items = []
    for k in keys:
        items.append("%s=%s" % (k, data[k]))

    data_string = '&'.join(items)

    h = hmac.new(secret_key, data_string, hashlib.sha256)

    return h.hexdigest()

if __name__ == '__main__':
    main()
