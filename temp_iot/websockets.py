# TODO IMPORTANT: ws4py requires a fix for compatibility with latest gevent.
# WSGI handler must wait for greenlet (joinall) before returning.
# see https://github.com/Lawouach/WebSocket-for-Python/pull/180/files

from gevent import monkey; monkey.patch_all()
from ..protocols import collector_pb2
from ..common import sensor_types
from ..common import config

from ws4py.server.geventserver import WSGIServer
from ws4py.server.wsgiutils import WebSocketWSGIApplication
from ws4py.websocket import WebSocket
import redis
import json

class SubscriberWebSocket(WebSocket):
    r = None
    p = None

    def opened(self):
        print "SubscribeWebSocket Connection opened"

        # TODO there really should be only one connection to Redis with data then distributed
        # to connected web sockets within this this process.
        redis_host = config.get('Redis', 'Host')
        redis_port = config.getint('Redis', 'port')

        self.r = redis.StrictRedis(host=redis_host, port=redis_port)
        self.p = self.r.pubsub()
        self.p.psubscribe("sensor_status.*")

        for message in self.p.listen():
            if self.terminated:
                break

            if message['type'] not in ['message', 'pmessage']:
                print "Ignoring message %s" % message
                continue

            try:
                # TODO: Note that this will attempt to parse any messages as bare SensorStatus objects.
                # Maybe we should define a new wrapper message type with oneof (like a tagged union).
                status = collector_pb2.SensorStatus.FromString(message['data'])
            except Exception as e:
                print "Error parsing redis message, %s" % e

            try:
                self.send(self._format_status_as_json(status, message['channel']))
            except Exception as e:
                print "Socket error: %s, closing connection." % e
                break

    def closed(self, code, reason=None):
        print "SubscribeWebSocket Connection closed: %d %s" % (code, reason)
        if self.p:
            self.p.close()

    def received_message(self, m):
        print "SubscribeWebSocket received a message, ignoring it. %s" % m

    def _format_status_as_json(self, status, channel):
        return json.dumps({
            'node_name': status.hostname,
            'sensor_type': sensor_types.sensor_type_name(status.sensor_type),
            'timestamp': status.timestamp,
            'value': round(status.float_value, 2) or status.int_value or status.bool_value,
            'channel': channel
        }, separators=(',', ': '))

def main():
    config.init('websockets')
    port = (config.get('Websockets', 'Host'), config.getint('Websockets', 'Port'))

    print "Websockets server starting up at %s:%d" % port

    server = WSGIServer(port, WebSocketWSGIApplication(handler_cls=SubscriberWebSocket))
    server.serve_forever()

if __name__ == '__main__':
    main()

